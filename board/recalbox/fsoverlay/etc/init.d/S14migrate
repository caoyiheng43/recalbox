#!/bin/bash

# This file runs migration scripts at each boot.
#
# Because they are run more than once, each migration must be *idempotent*.
# Running them at each boot ensures that even configurations stored on external
# USB drives will get migrated too.
#
# TODO: each migration should be in a separate timestamped file to ensure they
#       are executed in order and properly isolated. So far, they are just
#       declared in order, and assigned an incremental index (the currently
#       informative timestamp is generated with `date +%Y%m%d%H%M%S`).

# Run this script on startup only
if [[ $1 != "start" ]]; then
  exit 0
fi

# Useful global variables
_SHAREINIT="/recalbox/share_init"
_SHARE="/recalbox/share"
_ARCH=$(cat /recalbox/recalbox.arch)
INIT_SCRIPT=$(basename "$0")

# Significant files paths
recalboxConfPath="system/recalbox.conf"
esInputCfgPath="system/.emulationstation/es_input.cfg"

##########################
###     Migrations     ###
##########################

recallog -s "${INIT_SCRIPT}" -t "MIGRATION" "Running MIGRATIONS..."

### Migration 6 [20210617165703] Keep it until 8.1
### Copy all psp saves to the new directory : see https://gitlab.com/recalbox/recalbox/-/issues/1773
PSPSAVEDIR="/recalbox/share/saves/psp/ppsspp/PSP/"
mkdir -p "${PSPSAVEDIR}"

for SAVE_DIR in "SAVEDATA" "PPSSPP_STATE"; do
  if [ ! -z "$(ls -A /recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/)" ]; then
    recallog -s "${INIT_SCRIPT}" -t "MIGRATION-INFO" "MIGRATION of PPSSPP saves from /recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/ to /recalbox/share/saves/psp/ppsspp/PSP/"
    cp -r "/recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/" "${PSPSAVEDIR}" && \
    rm -rf "/recalbox/share/system/configs/ppsspp/PSP/${SAVE_DIR}/"
  fi
done
